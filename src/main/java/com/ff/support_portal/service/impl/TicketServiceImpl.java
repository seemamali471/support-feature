package com.ff.support_portal.service.impl;

import com.ff.support_portal.dao.TicketDao;
import com.ff.support_portal.dao.UserDao;
import com.ff.support_portal.dto.CommonConstants;
import com.ff.support_portal.dto.CustomerTicketDto;
import com.ff.support_portal.dto.Response;
import com.ff.support_portal.dto.ResponseDto;
import com.ff.support_portal.exception.NoTicketFoundException;
import com.ff.support_portal.model.Ticket;
import com.ff.support_portal.model.User;
import com.ff.support_portal.service.TicketService;
import com.ff.support_portal.util.ApiResponse;
import lombok.AllArgsConstructor;


import org.modelmapper.ModelMapper;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
@AllArgsConstructor
public class TicketServiceImpl implements TicketService {
    private final TicketDao ticketDao;
    private final UserDao userDao;
    private final MongoTemplate mongoTemplate;

    private final ModelMapper modelMapper;
    private final ResponseDto responseDto;


    @Override
    public Response getAllTickets(){
        List<Ticket> allTickets = ticketDao.getAllTickets();
        if(!allTickets.isEmpty()){
            return Response.builder().data(allTickets).statusCode(HttpStatus.OK.value()).httpStatus(HttpStatus.OK).build();
        }

        return Response.builder().statusCode(HttpStatus.NO_CONTENT.value()).httpStatus(HttpStatus.NO_CONTENT).build();
    }
    @Override
    public Response getAllTicketsByUserId(String userId){
//        List<Ticket> allTicketByUserId = ticketDao.getAllTicketByUserId(userId);
        Query query = new Query(Criteria.where(CommonConstants.CREATED_BY).is(userId));

        List<Ticket> allTicketByUserId = mongoTemplate.find(query, Ticket.class);
        if(!allTicketByUserId.isEmpty()){
            return Response.builder().statusCode(HttpStatus.OK.value()).data(allTicketByUserId).httpStatus(HttpStatus.OK).build();
        }


        return Response.builder().statusCode(HttpStatus.NO_CONTENT.value()).httpStatus(HttpStatus.NO_CONTENT).build();
    }

    @Override
    public Response saveTicket(CustomerTicketDto dto, String userEmail)  {

        User user = userDao.findUserByEmailId(userEmail);
        if(Objects.nonNull(user)){

            Ticket ticket = modelMapper.map(dto , Ticket.class);
            ticket.setId(UUID.randomUUID().toString());
            ticket.setCreateEntity(user.getId(), userEmail);
            Ticket savedTicket = ticketDao.saveTicket(ticket);

            return Response.builder().statusCode(HttpStatus.CREATED.value()).httpStatus(HttpStatus.CREATED).data(savedTicket).build();

        }

        return Response.builder().httpStatus(HttpStatus.NOT_FOUND).data("User Not Found with this email Id").statusCode(HttpStatus.NOT_FOUND.value()).build();
    }

    @Override
    public Response updateTicket(CustomerTicketDto customerTicketDto, String ticketId, String userEmail) {

        Ticket oldTicket = ticketDao.getTicketById(ticketId).orElseThrow(NoTicketFoundException::new);
        User user = userDao.findUserByEmailId(userEmail);

        if(Objects.nonNull(user)){

            modelMapper.getConfiguration().setSkipNullEnabled(true); // Skip null properties
            modelMapper.map(customerTicketDto, oldTicket);

            oldTicket.setParticipantsEmail(customerTicketDto.getParticipantsEmail());
            oldTicket.setShareWithEmail(customerTicketDto.getShareWithEmail());
            oldTicket.setComments(customerTicketDto.getComments());
            oldTicket.setModifiedEntity(user.getId(), userEmail);
            oldTicket = ticketDao.saveTicket(oldTicket);

            return Response.builder().statusCode(HttpStatus.OK.value()).data(oldTicket).httpStatus(HttpStatus.OK).build();

        }

        return null;

    }

    @Override
    public ResponseDto assignTickets(List<Ticket> tickets, User user) {
        tickets.forEach(ticket ->
            ticket.setAssignedTo(user)
        );

        if (user.getTickets().isEmpty()){
            user.setTickets(tickets);
            user.setAssignedTickets(tickets.size());
        } else {
            user.getTickets().addAll(tickets);
            user.setAssignedTickets(user.getTickets().size());
        }
        return ticketDao.assignTickets(tickets, user);
    }

    @Override
    public ResponseDto viewAllAssignee(String privilege) {

        Query query = new Query(Criteria.where(CommonConstants.PRIVILEGE).is(privilege));
        List<User> user = mongoTemplate.find(query, User.class, "user");
        responseDto.setResponseCode(HttpStatus.OK.value());
        responseDto.setObject(user);
        responseDto.setStatus(HttpStatus.OK.toString());
        responseDto.setMessage("Users with "+privilege+" privilege");
        return responseDto;
    }

    @Override
    public ResponseEntity<ApiResponse> searchUserToAssign(String name, int pageNo, int pageSize) {
        return ticketDao.searchUsersToAssign(name,pageNo,pageSize);
    }

    @Override
    public ResponseEntity<ApiResponse> deleteTicketById(String id) {
        return ticketDao.deleteTicketById(id);
    }

    @Override
    public ResponseDto getTicketById(String ticketId) {
        Ticket ticket = ticketDao.getTicketById(ticketId).orElseThrow(()-> new NoTicketFoundException("No Ticket Found"));
        responseDto.setObject(ticket);
        responseDto.setStatusCode(HttpStatus.FOUND.value());
        responseDto.setMessage("Ticket found");
        return responseDto;
    }
}
