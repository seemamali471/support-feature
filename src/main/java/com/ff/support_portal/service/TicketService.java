package com.ff.support_portal.service;

import com.ff.support_portal.dto.CustomerTicketDto;
import com.ff.support_portal.dto.Response;
import com.ff.support_portal.dto.ResponseDto;
import com.ff.support_portal.model.Ticket;
import com.ff.support_portal.model.User;
import com.ff.support_portal.util.ApiResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TicketService {

    public Response getAllTickets();

    public Response getAllTicketsByUserId(String userId);

    public ResponseDto getTicketById(String ticketId);

    public Response saveTicket(CustomerTicketDto dto, String userEmail);

    public Response updateTicket(CustomerTicketDto ticketDto, String ticketId, String userEmail);

    ResponseDto assignTickets(List<Ticket> tickets, User user);

    ResponseDto viewAllAssignee(String privilege);

    ResponseEntity<ApiResponse> searchUserToAssign(String name, int pageNo, int pageSize);

    ResponseEntity<ApiResponse> deleteTicketById(String id);
}
