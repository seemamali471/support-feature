package com.ff.support_portal.exception;

import com.ff.support_portal.dto.ResponseDto;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;


@ControllerAdvice
@AllArgsConstructor
public class ExceptionHandler {
    private final ResponseDto responseDto;
    @org.springframework.web.bind.annotation.ExceptionHandler(NoTicketFoundException.class)
    public ResponseDto noTicketFoundException(NoTicketFoundException exception){
        responseDto.setMessage(exception.getMessage());
        responseDto.setStatusCode(HttpStatus.NO_CONTENT.value());
        responseDto.setObject(null);
    return responseDto;
    }
}
