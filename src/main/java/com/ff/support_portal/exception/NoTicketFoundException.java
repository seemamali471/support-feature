package com.ff.support_portal.exception;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class NoTicketFoundException extends RuntimeException{
    public  String message = "";

    @Override
    public String getMessage() {
        return message;
    }
}
