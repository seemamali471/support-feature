package com.ff.support_portal.dao;

import com.ff.support_portal.model.User;

public interface UserDao {

    User findUserByEmailId(String userEmail);
}
