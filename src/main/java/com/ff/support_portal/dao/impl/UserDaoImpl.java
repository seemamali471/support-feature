package com.ff.support_portal.dao.impl;

import com.ff.support_portal.dao.UserDao;
import com.ff.support_portal.model.User;
import com.ff.support_portal.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class UserDaoImpl implements UserDao {

    private final UserRepository userRepository;

    @Override
    public User findUserByEmailId(String userEmail) {
        return userRepository.findByUserEmail(userEmail);
    }
}
