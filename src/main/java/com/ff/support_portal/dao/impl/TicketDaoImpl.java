package com.ff.support_portal.dao.impl;

import com.ff.support_portal.dao.TicketDao;
import com.ff.support_portal.dto.ResponseDto;
import com.ff.support_portal.model.Ticket;
import com.ff.support_portal.model.User;
import com.ff.support_portal.repository.TicketRepository;
import com.ff.support_portal.util.ApiResponse;
import com.ff.support_portal.util.ResponseUtil;
import com.ff.support_portal.util.SequenceUtil;
import com.mongodb.client.result.DeleteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class TicketDaoImpl implements TicketDao {

    public final String TICKET = "ticket";
    public final String USER = "user";

    public final String NO_USER_FOUND = "No Users Found";

    public final String USER_FOUND = "user found";

    public final String NO_TICKET_FOUND = "No Ticket Found";

    @Autowired
    public ResponseDto responseDto;

    @Autowired
    public SequenceUtil sequenceUtil;
    @Autowired
    private TicketRepository ticketRepository;

    private final MongoTemplate mongoTemplate;

    public TicketDaoImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public List<Ticket> getAllTickets() {

        return ticketRepository.findAll();

    }

    public List<Ticket> getAllTicketByUserId(String userId) {
        return ticketRepository.findBycreatedByName(userId);
    }


    public Ticket saveTicket(Ticket ticket) {

        ticket.setId(sequenceUtil.generateSequence("TIC", mongoTemplate));
        return ticketRepository.save(ticket);
    }

    public Optional<Ticket> getTicketById(String ticketId) {
        Optional<Ticket> byId = ticketRepository.findById(ticketId);
        return byId;
    }

    @Override
    public ResponseDto assignTickets(List<Ticket> tickets, User user) {

        mongoTemplate.save(tickets, TICKET);
        User savedUser = mongoTemplate.save(user, USER);

        responseDto.setResponseCode(HttpStatus.OK.value());
        responseDto.setObject(savedUser);
        responseDto.setStatus(HttpStatus.OK.toString());
        responseDto.setMessage("Tickets has been assigned to user");
        return responseDto;
    }

    @Override
    public ResponseEntity<ApiResponse> searchUsersToAssign(String name, int pageNo, int pageSize) {
        Query query = new Query(Criteria.where("previlage").nin("admin", "customer").andOperator(Criteria.where("name").regex(name, "i")));
        List<User> userList = mongoTemplate.find(query.with(PageRequest.of(pageNo, pageSize)), User.class);
//        if (Objects.nonNull(userList)) {
//            userList= userList.stream().filter(user -> !(user.getPrivilege().equalsIgnoreCase(CommonConstants.ADMIN) ||  user.getPrivilege().equalsIgnoreCase(CommonConstants.CUSTOMER))).collect(Collectors.toList());
//            return ResponseUtil.getFoundResponse(userList,USER_FOUND);
//        } else {
        return ResponseUtil.getNoContentResponse(new PageImpl<>(userList, PageRequest.of(pageNo, pageSize), userList.size()));
    }

@Override
public ResponseEntity<ApiResponse> deleteTicketById(String ticketId) {
    Query query = new Query(Criteria.where("id").is(ticketId));
    Ticket ticket = mongoTemplate.findOne(query, Ticket.class);
    if (Objects.nonNull(ticket)) {
        DeleteResult deleteResult = mongoTemplate.remove(ticket);
        return ResponseUtil.getOkResponse("Deleted successfully");
    } else {
        return ResponseUtil.getNoContentResponse("No ticket with the given id");
    }
}

}
