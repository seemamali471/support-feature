package com.ff.support_portal.dao;

import com.ff.support_portal.dto.ResponseDto;
import com.ff.support_portal.model.Ticket;
import com.ff.support_portal.model.User;
import com.ff.support_portal.util.ApiResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TicketDao {


    public List<Ticket> getAllTickets();


    public List<Ticket> getAllTicketByUserId(String userId);

    public Ticket saveTicket(Ticket ticket);

    public Optional<Ticket> getTicketById(String ticketId);

    ResponseDto assignTickets(List<Ticket> tickets, User user);

    ResponseEntity<ApiResponse> searchUsersToAssign(String name, int pageNo, int pageSize);

    ResponseEntity<ApiResponse> deleteTicketById(String id);
}
