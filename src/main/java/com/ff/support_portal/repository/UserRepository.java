package com.ff.support_portal.repository;

import com.ff.support_portal.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {

    User findByUserEmail(String useremail);
}
