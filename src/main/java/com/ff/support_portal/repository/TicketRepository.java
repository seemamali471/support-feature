package com.ff.support_portal.repository;

import com.ff.support_portal.model.Ticket;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketRepository extends MongoRepository<Ticket, String> {

    public List<Ticket> findBycreatedByName(String userId);
}
