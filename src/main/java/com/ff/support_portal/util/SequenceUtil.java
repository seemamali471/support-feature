package com.ff.support_portal.util;

import com.ff.support_portal.dto.CommonConstants;
import com.ff.support_portal.model.Sequence;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Update;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.Objects;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Component
public class SequenceUtil {

    private static final String SEQUENCE = "sequence";
    private static final String SEQ = "seq";

    public String generateSequence(String seqName, MongoTemplate mongoTemplate) {
        final int initValue = 10000;
        if (Objects.nonNull(mongoTemplate)) {
            Sequence counter = getAndModify(mongoTemplate);
            if(Objects.isNull(counter)){
                counter = getAndModify(mongoTemplate);
            }
            Long seq = !Objects.isNull(counter) ? (initValue + counter.getSeq()) : initValue;
            String specifiedSeq = entitySequenceGenerated(seqName);
            return specifiedSeq + seq;
        }
        return CommonConstants.EMPTY;
    }

    private Sequence getAndModify(MongoTemplate mongoTemplate) {
        return mongoTemplate.findAndModify(query(where("_id").is(SEQUENCE)),
                new Update().inc(SEQ, 1), options().returnNew(true).upsert(true),
                Sequence.class);
    }

    private String entitySequenceGenerated(String seqName) {
        String value = StringUtils.deleteWhitespace(seqName);
        if(value.length() <= 2){
            int varLength = value.length();
            char ch = value.charAt(varLength - 1);
            StringBuilder stringBuilder = new StringBuilder(value);
            while(varLength < 3) {
                stringBuilder.append(ch);
                varLength++;
            }
            value = stringBuilder.substring(0, 3);
        } else {
            value = value.substring(0, 3);
        }
        return value.toUpperCase(Locale.ROOT);
    }
}
