package com.ff.support_portal.util;

import lombok.Data;
import org.springframework.http.HttpStatus;
@Data
public class ApiResponse {
    HttpStatus httpStatus;
    int statusCode;
    Object response;

}
