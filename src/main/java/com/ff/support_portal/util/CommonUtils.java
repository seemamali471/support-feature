package com.ff.support_portal.util;

import java.time.LocalDateTime;
import java.time.ZoneId;

public class CommonUtils {

    public static String getCurrentTime(){

        return LocalDateTime.now().toString() +" "+ ZoneId.of("Asia/Kolkata").toString();
    }
}
