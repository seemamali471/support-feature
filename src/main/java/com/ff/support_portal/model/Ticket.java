package com.ff.support_portal.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.File;
import java.time.LocalDateTime;
import java.util.List;


@Document(collection = "tickets")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Ticket extends BaseEntity{

    @Id
    String id;
    String issueRelatedTo;
    String summary;
    String status;
    User assignedTo;
    String description;
    String priority;
    String severity;
    String license;
    Boolean licenseAccess;
    List<String> participantsEmail;
    List<Comment> comments;
    String assignee;
    String ticketType;
    List<String> shareWithEmail;
    String assigneeGroup;
    List<File> attachedFiles;
}
