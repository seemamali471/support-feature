package com.ff.support_portal.model;

import com.ff.support_portal.util.CommonUtils;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class BaseEntity {

    String createdOn;
    String lastUpdateOn;
    String createdByName;

    String createdByEmail;
    String modifiedByName;
    String modifiedByEmail;

    public void setCreateEntity(String userName,String userEmail){
        this.createdByEmail=userEmail;
        this.createdByName=userName;
        this.createdOn = CommonUtils.getCurrentTime();
    }

    public void setModifiedEntity(String userName, String userEmail){
        this.modifiedByEmail = userEmail;
        this.modifiedByName = userName;
        this.lastUpdateOn = CommonUtils.getCurrentTime();
    }
}
