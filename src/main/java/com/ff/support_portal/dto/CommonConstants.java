package com.ff.support_portal.dto;

public class CommonConstants {


    public static final String NO_CONTENT = "No Content Found";

    public static final String SUCCESS = "Success";

    public static final String CREATED_BY = "createdBy";

    public static final String PRIVILEGE ="privilege";
    public static final String EMPTY ="empty";

    public static final String DELETED="Deleted";

    public static final String ADMIN="Admin";

    public static final String CUSTOMER="Customer";
}
