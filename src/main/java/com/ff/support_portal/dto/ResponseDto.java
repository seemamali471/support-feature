package com.ff.support_portal.dto;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class ResponseDto {
    private String message ;
    private int statusCode;
    private Object object;
    private int responseCode;
    private int errorCode;
    private String status;
}
