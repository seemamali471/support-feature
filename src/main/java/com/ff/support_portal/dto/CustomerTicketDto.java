package com.ff.support_portal.dto;

import com.ff.support_portal.model.BaseEntity;
import com.ff.support_portal.model.Comment;
import com.ff.support_portal.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.File;
import java.util.Date;
import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerTicketDto extends BaseEntity {

    String issueRelatedTo;
    String summary;
    String status;
    String description;
    String priority;
    String severity;
    String license;
    Boolean licenseAccess;
    List<String> participantsEmail;
    List<Comment> comments;
    String assignee;
    String ticketType;
    List<String> shareWithEmail;
    String assigneeGroup;
    User assignedTo;
    List<File> attachedFiles;
}
