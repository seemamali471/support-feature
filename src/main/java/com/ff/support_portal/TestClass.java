package com.ff.support_portal;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.zone.ZoneRules;
import java.util.TimeZone;

public class TestClass {

    public static void main(String[] args) {

        System.out.println(LocalDateTime.now());
        System.out.println(LocalDateTime.now(ZoneId.of("Asia/Kolkata")));

        String currentTime = LocalDateTime.now().toString() + ZoneId.of("Asia/Kolkata").toString();
        System.out.println(currentTime);
    }
}
