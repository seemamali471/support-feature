package com.ff.support_portal.controller;

import com.ff.support_portal.dto.CustomerTicketDto;
import com.ff.support_portal.dto.ResponseDto;
import com.ff.support_portal.model.User;
import com.ff.support_portal.service.TicketService;
import com.ff.support_portal.util.ApiResponse;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.ff.support_portal.dto.Response;
import com.ff.support_portal.model.Ticket;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;

@RestController
@RequestMapping("api/v1/tickets")
public class TicketController {

    private final TicketService ticketService;

//    public TicketController(TicketService ticketService, MongoTemplate mongoTemplate) {
//        this.ticketService = ticketService;
//        this.mongoTemplate = mongoTemplate;
//    }

    @PostMapping
    public Response createTicket(@RequestBody CustomerTicketDto customerTicketDto , @RequestHeader String userEmail )  {
        return ticketService.saveTicket(customerTicketDto, userEmail);
    }
    @GetMapping({"/{ticketId}"})
    public ResponseDto getTicketById(@PathVariable String ticketId){
        return ticketService.getTicketById(ticketId);
    }

    @GetMapping("/viewAll")
    public Response getAllTickets(){

         return ticketService.getAllTickets();
    }

    @GetMapping("/viewTickets/{userId}")
    public Response getTicketByUserId(@PathVariable String userId){

        return ticketService.getAllTicketsByUserId(userId);

    }

    @PutMapping("/{useremail}/{ticketId}")
    public Response updateTicketByTicketId(@PathVariable String ticketId, @PathVariable("useremail") String userEmail, @RequestBody CustomerTicketDto customerTicketDto){
        return ticketService.updateTicket(customerTicketDto, ticketId, userEmail);

    }

    @PutMapping()
    public ResponseDto assignTickets(List<Ticket> tickets, User user){
        return ticketService.assignTickets(tickets, user);
    }

    @GetMapping("{privilege}")
    public ResponseDto viewAllAssignee(@PathVariable(required = true) String privilege){
        return ticketService.viewAllAssignee(privilege);
    }
    @GetMapping("/search")
    public ResponseEntity<ApiResponse> searchUsersToAssign(@RequestParam String name, @RequestParam int pageNo, @RequestParam int pageSize)
    {
        return ticketService.searchUserToAssign(name,pageNo,pageSize);
    }
    private final MongoTemplate mongoTemplate;
    public TicketController(TicketService ticketService, MongoTemplate mongoTemplate) {
        this.ticketService = ticketService;
        this.mongoTemplate = mongoTemplate;
    }
    @PostMapping("/create")
    public ResponseDto createUser(@RequestBody User user)
    {
        mongoTemplate.save(user);
        ResponseDto responseDto=new ResponseDto();
        responseDto.setObject(user);
        responseDto.setResponseCode(HttpStatus.CREATED.value());
        responseDto.setMessage("created");
        return responseDto;
    }

    @PostMapping("/createT")
    public ResponseDto createTicket(@RequestBody Ticket ticket)
    {
        mongoTemplate.save(ticket);
        ResponseDto responseDto=new ResponseDto();
        responseDto.setObject(ticket);
        responseDto.setResponseCode(HttpStatus.CREATED.value());
        responseDto.setMessage("created");
        return responseDto;
    }



    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> deleteTicketById(@PathVariable String id)
    {
        return ticketService.deleteTicketById(id);
    }



}
